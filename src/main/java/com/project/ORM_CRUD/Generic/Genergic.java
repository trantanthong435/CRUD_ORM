package com.project.ORM_CRUD.Generic;

import com.project.ORM_CRUD.model.Employee;
import com.project.ORM_CRUD.model.Enum.EmpStatusEnum;

import java.util.*;


public class Genergic {


    public static void main(String[] args) {
        Employee emp = new Employee(144L, "name 34", "img45", EmpStatusEnum.WORKING, null, null, null, null);
        Employee emp2 = new Employee(154L, "name 34", "img45", EmpStatusEnum.WORKING, null, null, null, null);
        Employee emp3 = new Employee(147L, "name 34", "img45", EmpStatusEnum.WORKING, null, null, null, null);

        List<Employee> ls = new ArrayList<>();
        ls.add(emp);
        ls.add(emp2);
        ls.add(emp3);


        //   test1(ls, ls).forEach(System.out::println);

        String[] s = {"ss", "aa", "gg"};
        List<String> lsStr = new ArrayList<>();
        lsStr.add("fff");
        lsStr.add("kkg");


        fromArrayToCollection(s, lsStr);
        System.out.println(lsStr);


        Long[] l = {45L, 66L, 34L, 22L};
        List<Long> longList = new LinkedList<>();
        longList.add(4L);
        longList.add(5L);
        longList.add(7L);


        fromArrayToCollection(l, longList);
        System.out.println(longList);




    }


    public static <T, U> List<T> test1(List<U> listU, List<T> listT) {
        //        System.out.println(listU);
        //        System.out.println(listT);

        return listT;
    }

    public static <T> void fromArrayToCollection(T[] a, Collection<T> c) {
        Collections.addAll(c, a);
    }

    public <T extends Number> List<T> fromArrayToList(T[] a) {
        return new LinkedList<>(Arrays.asList(a));
    }


}
