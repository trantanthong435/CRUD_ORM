package com.project.ORM_CRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ORM_CRUD_Application {

  public static void main(String[] args) {
    SpringApplication.run(ORM_CRUD_Application.class, args);
  }

}
