package com.project.ORM_CRUD.common;

import com.opencsv.CSVWriter;
import com.project.ORM_CRUD.model.AbstractCSVRecord;
import com.project.ORM_CRUD.model.Employee;
import com.project.ORM_CRUD.model.Enum.EmpStatusEnum;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CSVHelper {
    public static String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }


    public static List<Employee> csvToEmployees(InputStream is) {
        try {
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            CSVParser csvParser = new CSVParser(fileReader, CSVFormat.EXCEL.withHeader());


            List<Employee> list = new ArrayList<>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                Employee emp = new Employee();

                emp.setId(Long.valueOf(csvRecord.get("id")));
                emp.setName(csvRecord.get("name"));
                emp.setImage(csvRecord.get("image"));
                emp.setStatus(EmpStatusEnum.valueOf(csvRecord.get("status")));
                emp.setCreatedBy(csvRecord.get("createdBy"));
                emp.setUpdatedBy(csvRecord.get("updatedBy"));
                emp.setCreatedDate(LocalDate.parse(csvRecord.get("createdDate")));
                emp.setUpdatedDate(LocalDate.parse(csvRecord.get("updatedDate")));

                list.add(emp);
            }
            fileReader.close();
            csvParser.close();
            is.close();

            return list;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }

    public static <T extends AbstractCSVRecord> ByteArrayInputStream exportCSV(List<T> data, Class<T> clazz) {
        BufferedWriter bufferedWriter = null;
        CSVWriter csvWriter = null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(out));
            //            bufferedWriter.write("\uFEFF");

            csvWriter = new CSVWriter(bufferedWriter, ',', '\u0000', '\u0000', "\r\n");

            // write CSV header
            T instantRecord = data.stream().findAny().orElse(null);
            if (instantRecord == null) {
                instantRecord = clazz.getDeclaredConstructor().newInstance();
            }

            String[] header = instantRecord.getHeaderNames().toArray(new String[0]);

            if (header != null) {
                csvWriter.writeNext(header);
            }

            //write records
            for (T record : data) {
                String[] lineData = record.toArray();
                Arrays.stream(lineData).filter(CSVHelper::isNotNullOrWhiteSpace).forEach(e -> e = "");

                csvWriter.writeNext(lineData);
            }
            csvWriter.flush();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (Objects.nonNull(bufferedWriter)) {
                    bufferedWriter.close();
                }

                if (Objects.nonNull(csvWriter)) {
                    csvWriter.close();
                }
            } catch (IOException ignored) {
            }
        }
        return new ByteArrayInputStream(out.toByteArray());
    }


    public static boolean isNotNullOrWhiteSpace(String s) {
        return !StringUtils.hasText(s);
    }

}
