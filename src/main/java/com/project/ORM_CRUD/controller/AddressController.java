package com.project.ORM_CRUD.controller;

import com.project.ORM_CRUD.model.Address;
import com.project.ORM_CRUD.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @PostMapping("/create")
    public Address createAddress(Address address) {
        return addressService.createAddress(address);
    }

    @GetMapping("")
    public List<Address> getAddLibrary() {
        return addressService.getAddAddress();
    }

    @GetMapping("/address/{id}")
    public Address getById(@PathVariable("id") long id) {
        return addressService.findById(id);
    }

    @DeleteMapping("/address/{id}")
    public void deleteById(@PathVariable("id") long id) {
        addressService.deleteById(id);
    }

}
