package com.project.ORM_CRUD.controller;

import com.project.ORM_CRUD.model.Author;
import com.project.ORM_CRUD.model.request.AuthorCreatingRequest;
import com.project.ORM_CRUD.repository.AuthorRepository;
import com.project.ORM_CRUD.service.AuthorService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/author")
@Validated
public class AuthorController {

  @Autowired
  private AuthorService authorService;

  @Autowired
  private AuthorRepository authorRepository;

  @PostMapping("/create")
  public ResponseEntity<Author> saveAuthor(@RequestBody @Valid AuthorCreatingRequest authorCreatingRequest) {
    return ResponseEntity.ok(authorService.saveAuthor(authorCreatingRequest));
  }

  @DeleteMapping("/delete/{id}")
  public void deleteAuthor(@PathVariable("id") long id) {
    authorService.deleteAuthor(id);
  }

  @GetMapping("/find/{id}")
  public Author findById(@PathVariable("id") long id) {
    return authorService.findById(id);
  }

  @GetMapping
  public List<Author> getAllAuthor(){
    return authorRepository.findAll();
  }


}
