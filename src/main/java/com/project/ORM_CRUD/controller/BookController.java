package com.project.ORM_CRUD.controller;

import com.project.ORM_CRUD.model.Book;
import com.project.ORM_CRUD.model.request.BookCreatingRequest;
import com.project.ORM_CRUD.repository.BookRepository;
import com.project.ORM_CRUD.service.AuthorService;
import com.project.ORM_CRUD.service.BookService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/book")
@Validated
public class BookController {

  @Autowired
  private BookService bookService;
  @Autowired
  private AuthorService authorService;
  @Autowired
  private BookRepository bookRepository;

  @PostMapping(value = "/create")
  public ResponseEntity<Book> saveBook(@RequestBody @Valid BookCreatingRequest bookCreatingRequest) {
    return ResponseEntity.ok(bookService.saveBook(bookCreatingRequest));
  }

  @DeleteMapping("/delete/{id}")
  public void deleteBook(@PathVariable("id") long id) {
    bookService.deleteBook(id);
  }

  @GetMapping("/find/{id}")
  public Book findById(@PathVariable("id") long id) {
    return bookService.findById(id);
  }

  @GetMapping
  public List<Book> getAllBook() {
    return bookRepository.findAll();
  }


/*  @RequestMapping("/test")
  public String test() {
    return "OK";
  }*/

}
