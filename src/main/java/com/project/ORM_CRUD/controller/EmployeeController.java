package com.project.ORM_CRUD.controller;

import com.project.ORM_CRUD.model.response.BaseCSVResponse;
import com.project.ORM_CRUD.model.response.DownloadCSVResponse;
import com.project.ORM_CRUD.repository.EmployeeRepository;
import com.project.ORM_CRUD.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeSerivce;
    private final EmployeeRepository employeeRepository;

    @PostMapping(value = "/importCSV", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<BaseCSVResponse> importFileCSV(@RequestParam("file") MultipartFile file) {
        //request.setFile(file);
        try {
            employeeSerivce.importFileCSV(file);
            return ResponseEntity.ok(employeeSerivce.importFileCSV(file));
        } catch (IOException e) {

        }
        return null;
    }

    @GetMapping(value = "/downloadCSV")
    public ResponseEntity<Resource> downloadCSV() {

        DownloadCSVResponse response = employeeSerivce.downloadCSV();

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + response.getFileName()).contentType(
                MediaType.parseMediaType("application/csv")).body(response.getFile());

    }


}
