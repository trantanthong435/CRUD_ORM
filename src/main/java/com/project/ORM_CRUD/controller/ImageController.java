package com.project.ORM_CRUD.controller;

import com.project.ORM_CRUD.model.Image;
import com.project.ORM_CRUD.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/image")
public class ImageController {
    @Autowired
    private ImageService imageService;

    @PostMapping("/create")
    public void saveImage(@RequestBody Image image) {
        imageService.saveImage(image);
    }

    @GetMapping("/find/{id}")
    public Image findById(long id) {
       return imageService.getById(id);
    }

    @GetMapping("")
    public List<Image> getAllImage() {
        return imageService.getAllImage();
    }

    @DeleteMapping("/delete/{id}")
    public void deleteImageById(long id){
        imageService.deleteById(id);
    }
}
