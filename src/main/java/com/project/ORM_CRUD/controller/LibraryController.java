package com.project.ORM_CRUD.controller;

import com.project.ORM_CRUD.model.Library;
import com.project.ORM_CRUD.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/library")
public class LibraryController {

    @Autowired
    private LibraryService libraryService;


    // Neu la @Controller thi de @ResponseBody o api  / @RESTController THI KO CAN
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Library createLibrary(@RequestBody @Valid Library library) {
        return libraryService.saveLibrary(library);
    }

    @GetMapping("/all")
    public List<Library> getAddLibrary() {
        return libraryService.getAllLibrary();
    }

    @GetMapping("/library/{id}")
    public Library getById(@PathVariable("id") long id) {
        return libraryService.getById(id);
    }

    @DeleteMapping("/library/{id}")
    public void deleteById(@PathVariable("id") long id) {
        libraryService.deleteById(id);
    }
}
