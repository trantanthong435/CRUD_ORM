package com.project.ORM_CRUD.exceptionhandler;

import com.project.ORM_CRUD.model.response.Response;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorException extends RuntimeException {
  private Response response;
}
