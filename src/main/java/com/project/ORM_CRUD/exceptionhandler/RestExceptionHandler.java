package com.project.ORM_CRUD.exceptionhandler;

import com.project.ORM_CRUD.model.response.Response;
import java.util.Iterator;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class RestExceptionHandler {

  @ResponseBody
  @ExceptionHandler(value = ErrorException.class)
  protected Response handleErrorException(ErrorException ex, HttpServletResponse response) {
    response.setStatus(ex.getResponse().getStatus());
    return ex.getResponse();
  }

  @ResponseBody
  @ExceptionHandler(value = MethodArgumentNotValidException.class)
  protected Response handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
    HttpServletResponse response) {

    StringBuilder message = new StringBuilder();
    Iterator<FieldError> fieldErrorIterator = ex.getBindingResult().getFieldErrors().iterator();

    while (fieldErrorIterator.hasNext()) {
      FieldError fieldError = fieldErrorIterator.next();
      message.append(fieldError.getField())
          .append(" ")
          .append(fieldError.getDefaultMessage());

      if (fieldErrorIterator.hasNext()) {
        message.append("; ");
      }
    }

    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

    return Response.getErrorResponse(
        HttpServletResponse.SC_BAD_REQUEST, message.toString(), null
    );
  }

}
