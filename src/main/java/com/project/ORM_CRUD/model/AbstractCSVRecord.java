package com.project.ORM_CRUD.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public abstract class AbstractCSVRecord {

    private Long id;
    private int lineNumber;
    private String errorMessage;
    // enum edit mode


    public abstract List<String> getHeaderNames();

    public abstract String[] toArray();

}
