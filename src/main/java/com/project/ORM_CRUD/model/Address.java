package com.project.ORM_CRUD.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity()
@Table(name = "address")
@Getter
@Setter
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "addressLocation")
    private String location;


    @OneToOne(mappedBy = "address")
    private Library library;

}
