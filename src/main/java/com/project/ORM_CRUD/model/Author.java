package com.project.ORM_CRUD.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Setter
@Getter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String email;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")  // id of avatar
    private Image avatar;

    // @JsonBackReference
    @ManyToMany(mappedBy = "authors")
    @OrderBy("id")
    private Set<Book> books = new LinkedHashSet<>();


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Book)) {
            return false;
        }
        Author author = (Author) o;
        return StringUtils.equals(name, author.getName()) && StringUtils.equals(email, author.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email);
    }

}