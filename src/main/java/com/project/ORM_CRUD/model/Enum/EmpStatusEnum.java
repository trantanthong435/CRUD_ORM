package com.project.ORM_CRUD.model.Enum;

public enum EmpStatusEnum {
    WORKING(1),
    NOT_WORKING(2);

    private final int value;

    public int getValue() {
        return value;
    }

    EmpStatusEnum(int status) {
        this.value = status;
    }
}
