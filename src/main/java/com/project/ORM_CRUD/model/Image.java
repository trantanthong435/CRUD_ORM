package com.project.ORM_CRUD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    private String data; // or byte[] | important field | use to contain data of img

    @OneToOne(mappedBy = "avatar")  // map by avatar line 17
    private Author author;

    @OneToOne(mappedBy = "photoBook")
    private Book book;

}
