package com.project.ORM_CRUD.model.request;

import com.project.ORM_CRUD.model.Book;
import java.util.Set;
import javax.validation.constraints.NotBlank;

import com.project.ORM_CRUD.model.Image;
import lombok.Data;

@Data
public class AuthorCreatingRequest {

  @NotBlank
  private String name;

  @NotBlank
  private String email;

  private Set<Book> books;

  private Image image;
}
