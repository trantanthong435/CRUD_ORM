package com.project.ORM_CRUD.model.request;

import com.project.ORM_CRUD.model.Author;
import java.util.Set;
import javax.validation.constraints.NotBlank;

import com.project.ORM_CRUD.model.Image;
import lombok.Data;

@Data
public class BookCreatingRequest {

  @NotBlank
  private String name;

  private Set<Author> authors;

  private Image image;
}
