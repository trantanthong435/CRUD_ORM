package com.project.ORM_CRUD.model.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadCSVRequest {

    private MultipartFile file;
    private String note;
}
