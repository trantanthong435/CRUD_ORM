package com.project.ORM_CRUD.model.response;

import com.project.ORM_CRUD.model.AbstractCSVRecord;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
//@EqualsAndHashCode(callSuper = true)
public class BaseCSVResponse<T extends AbstractCSVRecord> {

    private int total;
    private int successTotal;
    private int failTotal;
    private int createdTotal;
    private int updateTotal;
    private List<T> failRecords = new ArrayList<>();
}
