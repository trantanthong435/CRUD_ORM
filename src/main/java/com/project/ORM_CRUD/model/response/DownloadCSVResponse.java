package com.project.ORM_CRUD.model.response;

import lombok.Data;
import org.springframework.core.io.InputStreamResource;

@Data
public class DownloadCSVResponse {

    private InputStreamResource file;
    private String fileName;
}
