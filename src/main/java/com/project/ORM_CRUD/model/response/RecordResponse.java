package com.project.ORM_CRUD.model.response;

import com.project.ORM_CRUD.model.AbstractCSVRecord;
import com.project.ORM_CRUD.model.Enum.EmpStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RecordResponse extends AbstractCSVRecord {
    private Long id;
    private String name;

    private String image;

    private EmpStatusEnum status;

    private LocalDate createdDate;

    private String createdBy;

    private String updatedBy;

    private LocalDate updatedDate;


    @Override
    public List<String> getHeaderNames() {
        return Collections.singletonList("id , name,image, status, createdDate, createdBy, updatedBy, updatedDate");
    }

    @Override
    public String[] toArray() {
        String[] s = new String[8];
        s[0] = id.toString();
        s[1] = name;
        s[2] = image;
        s[3] = status.toString();
        s[4] = createdDate.toString();
        s[5] = createdBy;
        s[6] = updatedBy;
        s[7] = updatedDate.toString();

        return s;
    }
}
