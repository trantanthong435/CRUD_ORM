package com.project.ORM_CRUD.model.response;

import com.project.ORM_CRUD.common.Constant;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
  private int status;
  private String message;
  private Object data;

  public static Response getSuccessResponse(Object data) {
    return new Response(HttpServletResponse.SC_OK, Constant.EMPTY_STRING, data);
  }

  public static Response getErrorResponse(int status, String message, Object data) {
    return new Response(status, message, data);
  }

}
