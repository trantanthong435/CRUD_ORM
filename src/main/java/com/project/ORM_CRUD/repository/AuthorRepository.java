package com.project.ORM_CRUD.repository;

import com.project.ORM_CRUD.model.Author;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
  Optional<Author> findByName(String name);
  Optional<Author> findByNameAndEmail(String name, String email);
}
