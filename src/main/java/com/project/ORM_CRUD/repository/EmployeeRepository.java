package com.project.ORM_CRUD.repository;

import com.project.ORM_CRUD.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
