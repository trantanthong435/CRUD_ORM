package com.project.ORM_CRUD.service;

import com.project.ORM_CRUD.model.Address;
import com.project.ORM_CRUD.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public Address createAddress(Address address) {
        return addressRepository.save(address);
    }

    public List<Address> getAddAddress() {
        return addressRepository.findAll();
    }

    public void deleteById(long id) {
        addressRepository.deleteById(id);
    }

    public Address findById(long id) {
        return addressRepository.getById(id);
    }

}
