package com.project.ORM_CRUD.service;

import com.project.ORM_CRUD.exceptionhandler.ErrorException;
import com.project.ORM_CRUD.model.Author;
import com.project.ORM_CRUD.model.Book;
import com.project.ORM_CRUD.model.request.AuthorCreatingRequest;
import com.project.ORM_CRUD.model.response.Response;
import com.project.ORM_CRUD.repository.AuthorRepository;
import com.project.ORM_CRUD.repository.BookRepository;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {

  @Autowired
  AuthorRepository authorRepository;
  @Autowired
  BookRepository bookRepository;

  public Author saveAuthor(AuthorCreatingRequest authorCreatingRequest) {
    authorRepository.findByNameAndEmail(
        authorCreatingRequest.getName(), authorCreatingRequest.getEmail()
    ).ifPresent(e -> {
      throw new ErrorException(
          Response.getErrorResponse(
              HttpServletResponse.SC_BAD_REQUEST,
              String.format(
                  "An author with name %s and email %s existed",
                  authorCreatingRequest.getName(),
                  authorCreatingRequest.getEmail()
              ),
              null
          )
      );
    });

    Author newAuthor = new Author();
    newAuthor.setName(authorCreatingRequest.getName());
    newAuthor.setEmail(authorCreatingRequest.getEmail());

    if (authorCreatingRequest.getBooks() != null) {
      authorCreatingRequest.getBooks().forEach(e -> {
        Optional<Book> bookOptional = bookRepository.findByName(e.getName());
        Book book = null;
        if (bookOptional.isPresent()) {
          book = bookOptional.get();
          if (book.getAuthors() == null) {
            book.setAuthors(new LinkedHashSet<>());
          }
          book.getAuthors().add(newAuthor);
        } else {
          book = new Book();
          book.setName(e.getName());
          book.setAuthors(Collections.singleton(newAuthor));

          bookRepository.save(book);
        }
        newAuthor.getBooks().add(book);
      });
    }

    if(authorCreatingRequest.getImage() != null){
      newAuthor.setAvatar(authorCreatingRequest.getImage());
    }

    return authorRepository.save(newAuthor);
  }

  public Author findById(long id) {
    Optional<Author> author = authorRepository.findById(id);
    if (author.isPresent()) {
      return author.get();
    }
    throw new ErrorException(Response.getErrorResponse(
        HttpServletResponse.SC_BAD_REQUEST,
        "No author with id " + id, null));
  }

  public Author findByName(String name) {
    Optional<Author> author = authorRepository.findByName(name);
    if (author.isPresent()) {
      return author.get();
    }
    throw new ErrorException(Response.getErrorResponse(
        HttpServletResponse.SC_BAD_REQUEST,
        "No author with name " + name, null));
  }

  public void deleteAuthor(long id) {
    authorRepository.deleteById(id);
  }

}
