package com.project.ORM_CRUD.service;

import com.project.ORM_CRUD.exceptionhandler.ErrorException;
import com.project.ORM_CRUD.model.Author;
import com.project.ORM_CRUD.model.Book;
import com.project.ORM_CRUD.model.Image;
import com.project.ORM_CRUD.model.request.BookCreatingRequest;
import com.project.ORM_CRUD.model.response.Response;
import com.project.ORM_CRUD.repository.AuthorRepository;
import com.project.ORM_CRUD.repository.BookRepository;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

  @Autowired
  private BookRepository bookRepository;
  @Autowired
  private AuthorRepository authorRepository;

  public Book saveBook(BookCreatingRequest bookCreatingRequest) {
    bookRepository.findByName(bookCreatingRequest.getName())
      .ifPresent(e -> {
        throw new ErrorException(
            Response.getErrorResponse(
                HttpServletResponse.SC_BAD_REQUEST,
                String.format("A book with name %s existed", bookCreatingRequest.getName()),
                null
            )
        );
    });

    Book newBook = new Book();
    newBook.setName(bookCreatingRequest.getName());

    LinkedHashSet<Author> authors = new LinkedHashSet<>();

    if (bookCreatingRequest.getAuthors() != null) {
      bookCreatingRequest.getAuthors().forEach(e -> {
        Optional<Author> authorOptional = authorRepository.findByNameAndEmail(e.getName(), e.getEmail());
        Author author = null;
        if (authorOptional.isPresent()) {
          author = authorOptional.get();
          if (author.getBooks() == null) {
            author.setBooks(new LinkedHashSet<>());
          }
          author.getBooks().add(newBook);
        } else {
          author = new Author();
          author.setName(e.getName());
          author.setEmail(e.getEmail());
          author.setBooks(Collections.singleton(newBook));
        }
        authors.add(author);
      });
      newBook.setAuthors(authors);
    }

    if (bookCreatingRequest.getImage() != null){
      newBook.setPhotoBook(bookCreatingRequest.getImage());
    }

    return bookRepository.save(newBook);
  }

  public Book findById(long id) {
    Optional<Book> book = bookRepository.findById(id);
    if (book.isPresent()) {
      return book.get();
    }
    throw new ErrorException(Response.getErrorResponse(
        HttpServletResponse.SC_BAD_REQUEST,
        "No book with id " + id, null));
  }

  public Book findByName(String name) {
    Optional<Book> book = bookRepository.findByName(name);
    if (book.isPresent()) {
      return book.get();
    }
    throw new ErrorException(Response.getErrorResponse(
        HttpServletResponse.SC_BAD_REQUEST,
        "No book with name " + name, null));
  }

  public void deleteBook(long id) {
    Book book = bookRepository.findById(id).get();
    if (book != null) {
      bookRepository.delete(book);
    }
  }

}
