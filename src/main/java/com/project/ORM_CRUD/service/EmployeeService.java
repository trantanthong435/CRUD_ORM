package com.project.ORM_CRUD.service;

import com.project.ORM_CRUD.model.request.UploadCSVRequest;
import com.project.ORM_CRUD.model.response.BaseCSVResponse;
import com.project.ORM_CRUD.model.response.DownloadCSVResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface EmployeeService {
    void getAllEmp();

    BaseCSVResponse importFileCSV(UploadCSVRequest request) throws IOException;

    BaseCSVResponse importFileCSV(MultipartFile file) throws IOException;

    DownloadCSVResponse downloadCSV();
}
