package com.project.ORM_CRUD.service;

import com.project.ORM_CRUD.model.Image;
import com.project.ORM_CRUD.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageService {

   @Autowired
    private ImageRepository imageRepository;

    public Image saveImage(Image image){
        return imageRepository.save(image);
    }

    public Image getById(long id){
        return imageRepository.getById(id);
    }

    public List<Image> getAllImage(){
        return imageRepository.findAll();
    }

    public void deleteById(long id){
        imageRepository.deleteById(id);
    }
}
