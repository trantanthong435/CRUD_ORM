package com.project.ORM_CRUD.service.Impl;

import com.project.ORM_CRUD.common.CSVHelper;
import com.project.ORM_CRUD.model.Employee;
import com.project.ORM_CRUD.model.request.UploadCSVRequest;
import com.project.ORM_CRUD.model.response.BaseCSVResponse;
import com.project.ORM_CRUD.model.response.DownloadCSVResponse;
import com.project.ORM_CRUD.model.response.RecordResponse;
import com.project.ORM_CRUD.repository.EmployeeRepository;
import com.project.ORM_CRUD.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;


    @Override
    public void getAllEmp() {
        employeeRepository.findAll();
    }

    @Override
    public BaseCSVResponse importFileCSV(UploadCSVRequest request) throws IOException {

        //        List<Employee> list = CSVHelper.csvToTutorials(request.getFile());
        //        employeeRepository.saveAll(list);
        return null;
    }

    @Override
    public BaseCSVResponse importFileCSV(MultipartFile file) throws IOException {
        List<Employee> list = CSVHelper.csvToEmployees(file.getInputStream());
        employeeRepository.saveAll(list);
        return null;
    }

    @Override
    public DownloadCSVResponse downloadCSV() {
        List<Employee> list = employeeRepository.findAll();
        List<RecordResponse> recordResponses = new ArrayList<>();

        list.forEach(e -> {
            RecordResponse response = new RecordResponse();

            response.setId(e.getId());
            response.setName(e.getName());
            response.setImage(e.getImage());
            response.setStatus(e.getStatus());
            response.setCreatedBy(e.getCreatedBy());
            response.setCreatedDate(e.getCreatedDate());
            response.setUpdatedBy(e.getUpdatedBy());
            response.setUpdatedDate(e.getUpdatedDate());

            recordResponses.add(response);
        });

        ByteArrayInputStream inputStream = CSVHelper.exportCSV(recordResponses, RecordResponse.class);
        String fileName = "emp.csv";

        DownloadCSVResponse downloadCSVResponse = new DownloadCSVResponse();
        downloadCSVResponse.setFile(new InputStreamResource(inputStream));
        downloadCSVResponse.setFileName(fileName);

        return downloadCSVResponse;
    }
}
