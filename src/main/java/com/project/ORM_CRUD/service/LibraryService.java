package com.project.ORM_CRUD.service;

import com.project.ORM_CRUD.model.Library;
import com.project.ORM_CRUD.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibraryService {

    @Autowired
    private LibraryRepository libraryRepository;

    public Library saveLibrary(Library library) {
        return libraryRepository.save(library);
    }

    public List<Library> getAllLibrary() {
        return libraryRepository.findAll();
    }

    public Library getById(long id){
        return libraryRepository.getById(id);
    }

    public void deleteById(long id){
        libraryRepository.deleteById(id);
    }

}
